//
// Created by erucq on 06-08-18.
//

#include "Game.h"

void Game::choixPersonnage() {
    cout << "Avant de commencer l'aventure, choisis ta classe :" << endl;
    cout << "1. Elfe" << endl << "2. Nain" << endl << "3. Humain" << endl << "4. Geant" << endl << "5. Gobelin" << endl;

    string input;

    int choix = 0;

    Elfe *elf;
    Nain *nain;
    Humain *humain;
    Geant *geant;
    Gobelin *gob;

    while (choix < 1 || choix > 5) {
        cin >> input;

        choix = std::atoi(input.c_str());

        cout << "Ton choix : " << choix;

        switch (choix) {
            case 1:
                elf = new Elfe();
                this->hero = elf;
                break;
            case 2:
                nain = new Nain();
                this->hero = nain;
                break;
            case 3:
                humain = new Humain();
                this->hero = humain;
                break;
            case 4:
                geant = new Geant();
                this->hero = geant;
                break;
            case 5:
                gob = new Gobelin();
                this->hero = gob;
                break;
            default:
                cout << "Choisir entre 1 et 5 tchanbite" << endl;
                break;
        }

        cout << choix << " choix" << endl;
    }
    this->hero->printStats();
    this->hero->ctx = this->ctx;

}

int Game::createGame() {
    int dungeonResult = 0;

    string input;

    int continueGame = 1;
    while (continueGame == 1 && hero->hp > 0)  {
        dungeonResult = 0;
        this->showActionsMenu();

        cin >> input;

        if (input == "stop") {
            continueGame = 0;
        }
        if (input == "go") {
            dungeonResult = this->actionStartDungeon();
            if (dungeonResult == FIGHT_RESULT_LOSE) {
                continueGame = 0;
            }
        }
        if (input == "inv") {
            hero->seeInventory();
        }
        if (input == "c") {
            hero->printStats();
        }

    }

    return (0);
}

void Game::showActionsMenu() {
    cout << endl << "Que souhaites-tu faire ?" << endl << endl;
    cout << "go: Démarrer un donjon" << endl;
    cout << "buy: Accès au marchand" << endl;
    cout << "inv: Inventaire" << endl;
    cout << "c: Caracteristiques" << endl;
    cout << "save: Sauvegarder" << endl;
    cout << "stop: Abandonner" << endl;

}

void Game::actionMove() {

}

void Game::actionInventory() {
    this->hero->inv->print();
}

int Game::actionStartDungeon() {
    int ending = 0;
    cout << "Vous entrez dans un donjon" << endl;

    cout << "Vous devez battre le gardien de la porte du donjon pour pouvoir continuer" << endl;

    Mob *gardien = new GardienBasic();

    int resultFight = fight(this->hero, gardien);

    if (resultFight == 4) {
        cout << "Mort" << endl;
        return (4);
    }

    if (resultFight == 5) {
        cout << "Abandonné" << endl;
        return (5);
    }

    cout << "Vous entrez dans le donjon..." << endl;

    int dungeonProcess = this->dungeonLoop();
    return (dungeonProcess);

}

int Game::dungeonLoop() {
    string input;
    int continueDungeon = 1;

    int lastRoom = 0;
    int currentRoom = 0;

    while (this->hero->hp > 0 && continueDungeon == 1) {

        this->showDungeonMenu();

        cin >> input;

        if (input == "z") {
            // Move top
            this->nextRoomEvent();
        }
        if (input == "q") {
            // Move left
            this->nextRoomEvent();
        }
        if (input == "s") {
            // Move bottom
            this->nextRoomEvent();
        }
        if (input == "d") {
            // Move right
            this->nextRoomEvent();
        }
        if (input == "e") {
            // Open the chest
        }
        if (input == "i") {
            // See inventory
            this->hero->seeInventory();
        }
        if (input == "c") {
            hero->printStats();
        }
    }

    return (1);
}

void Game::showDungeonMenu() {
    cout << "___________________________________________" << endl;
    cout << "|                                         |" << endl;
    cout << "___________________________________________" << endl << endl;
    cout << "z. Move top" << endl;
    cout << "s. Move bottom" << endl;
    cout << "q. Move left" << endl;
    cout << "z. Move right" << endl;
    cout << "e. Use / Open" << endl;
    cout << "i. Inventory" << endl;
    cout << "c. Caracteristiques" << endl;

}

int Game::nextRoomEvent () {
    int result = 0;
    int evt = rand() % 8;

    if (evt <= 5) {
        Mob *mob = Mob::random();
        cout << "Vous tombez nez a nez avec un " << mob->name << " !" << endl;
        result = fight(this->hero, mob);
    }
    else {
        cout << "Vous trouvez un coffre !" << endl;
        int clef = this->hero->inv->check(201);
        if (clef >= 0) {
            this->hero->inv->removeOne(201);
            cout << "Vous avez ouvert le coffre avec une clef se trouvant dans votre sac" << endl;
            this->hero->openChest();
        } else {
            cout << "Ce coffre requiert une clef pour etre ouvert... Tant pis " << clef << endl;
        }
    }

    return (result);
}

