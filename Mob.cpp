//
// Created by erucq on 06-08-18.
//

#include "Mob.h"

Mob::Mob()
{

}

Mob::Mob(int hp_, int mp_, int damage_, int bouclier_) : hp(hp_), mp(mp_), damage(damage_), bouclier(bouclier_)
{

}

Mob::Mob(int hp_, int mp_, int damage_, int bouclier_, int max_money, int max_loot) : hp(hp_), mp(mp_), damage(damage_), bouclier(bouclier_), max_money(max_money), max_loot(max_loot)
{

}

Mob* Mob::random() {
    Mob *mob;
    int randomMob = rand() % lastMob;

    if (randomMob == tGnome) {
        Gnome *gnome = new Gnome();
        mob = gnome;
    }
    else if (randomMob == tGnomeNoir) {
        GnomeNoir *gnoir = new GnomeNoir();
        mob = gnoir;
    }
    else if (randomMob == tGoule) {
        Goule *goule = new Goule();
        mob = goule;
    }

    return (mob);
}

int Mob::receiveDamage(int dmg) {
    dmg = dmg - this->bouclier;

    if (dmg < 0) {
        dmg = 0;
    }

    this->hp -= dmg;

    if (this->hp < 0) {
        this->hp = 0;
    }
    return (this->hp);
}

GardienBasic::GardienBasic() : Mob(20, 0, 12, 0, 5, 1)
{
    this->name = (string)"Gardien Basique";
    Item *it = Item::generate(201);
    this->inv->add(it);
}

GnomeNoir::GnomeNoir() : Mob(40, 10, 12, 1, 15, 3)
{
    this->name = (string)"Gnome Noir";
}

Gnome::Gnome() : Mob(30, 10, 12, -1, 10, 1)
{
    this->name = (string)"Gnome";
}

Goule::Goule() : Mob(35, 10, 12, 0, 8, 2)
{
    this->name = (string)"Goule";
}


