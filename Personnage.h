//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_PERSONNAGE_H
#define RPGGAME_PERSONNAGE_H

#include "Context.h"
#include "Item.h"
#include "Inventory.h"
#include "Mob.h"
#include "Spell.h"

class Personnage {

public:
    int hp = 0;
    int max_hp = 0;
    int mp = 0;
    int max_mp = 0;
    Inventory *inv = new Inventory();
    int money = 10;

    int power = 0;

    int bouclier = 0;
    int damage = 0;

    Context *ctx;

    Personnage * create();
    Personnage(int hp_, int mp_);

    void coupDePoing(Mob *mob);
    int receiveDamage(int dmg);
    void receiveAttaque(Mob *mob);
    void receiveDrop(Mob *mob);
    void useItem(int choix);
    void usePotion(Potion *potion);
    void useParchemin(Parchemin *parcho);
    void seeInventory();
    void printStats();
    virtual int cast(Mob *mob);
    void launchCast(Spell *spell, Mob *mob);
    void launchCastHeal(Spell *spell);
    int receiveHeal(int n);
    void openChest();
};

class Elfe : public Personnage {
public:

    Elfe * create();
    Elfe();
    virtual int cast(Mob *mob);
};

class Nain : public Personnage {
public:

    Nain * create();
    Nain();
    virtual int cast(Mob *mob);
};

class Humain : public Personnage {
public:

    Humain * create();
    Humain();
    virtual int cast(Mob *mob);
};

class Geant : public Personnage {
public:

    Geant * create();
    Geant();
    virtual int cast(Mob *mob);
};

class Gobelin : public Personnage {
public:

    Gobelin * create();
    Gobelin();
    virtual int cast(Mob *mob);
};


#endif //RPGGAME_PERSONNAGE_H
