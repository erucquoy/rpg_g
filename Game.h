//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_GAME_H
#define RPGGAME_GAME_H

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <vector>
#include <map>

#include "Context.h"
#include "Inventory.h"
#include "Item.h"
#include "Personnage.h"
#include "Mob.h"
#include "fight.h"

using namespace std;

class Game {

private:
    Personnage* hero;
    Mob *mob;

public:
    Context *ctx;
    int inFight = 0;
    int inDungeon = 0;
    int salleDungeon = 0;
    int createGame ();
    int nbDungeon = 0;
    map<int, string> listItems;
    vector<int> listItemId;
    void loadItem(vector<string> item);
    Item *generateItem();
    void choixPersonnage();

protected:
    void showActionsMenu();
    void actionMove();
    void actionInventory();
    int actionStartDungeon();
    void showDungeonMenu();
    int dungeonLoop();
    int nextRoomEvent();

};


#endif //RPGGAME_GAME_H
