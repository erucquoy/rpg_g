//
// Created by erucq on 06-08-18.
//

#include "fight.h"

int fight (Personnage *hero, Mob *mob) {
    string input;
    int giveup = 0;
    int resFinal = 0;

    while ((hero->hp > 0 && mob->hp > 0) && giveup == 0) {

        showActions();

        cin >> input;

        if (input == "a") {
            hero->coupDePoing(mob);
        }

        if (input == "f") {
            hero->cast(mob);
        }

        if (input == "e") {
            hero->seeInventory();
        }

        if (input == "giveup") {
            int giveupResult = tryToGiveUp(hero, mob);
            if (giveupResult == 1) {
                giveup = 1;
                cout << "Vous arrivez à vous echappez !" << endl;


            } else {
                cout << "Vous n'arrivez pas à vous echappez..." << endl;
            }
        }

        if (mob->hp > 0) {
           hero->receiveAttaque(mob);
        }
    }

    if (hero->hp > 0 && mob->hp == 0) {
        cout << "Combat terminé... " << mob->name << " a succombé" << endl;
        resFinal = FIGHT_RESULT_WIN;
        hero->receiveDrop(mob);
    }

    if (hero->hp == 0) {
        resFinal = FIGHT_RESULT_LOSE;
    }

    if (giveup == 1) {
        resFinal = FIGHT_RESULT_GIVEUP;
    }

    return (resFinal);
}

void showActions() {
    cout << "Choisissez votre action :" << endl;
    cout << "a. Corp a corp" << endl;
    cout << "f. Lancer un sort" << endl;
    cout << "e. Utiliser un objet" << endl;
    cout << "giveup. Fuite" << endl;
}

int tryToGiveUp(Personnage *hero, Mob *mob) {

    int result = 0;

    srand(time(NULL));

    result = rand() % 2;

    return (result);
}