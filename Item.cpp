//
// Created by erucq on 06-08-18.
//

#include "Item.h"

vector<int> Item::listItemId;
map<int, string> Item::listItems;

Item::Item() {

}

Item::Item(int id) : id(id) {

}

Item::Item(int id, string name) : id(id) {
    this->name = name;
}

Item::Item(int id, string name, string type) : id(id), type(type) {
    this->name = name;
}


Item::Item(int id, string name, string type, int usable) : id(id), type(type), usable(usable) {
    this->name = name;
}

string Item::toStr() {
        string str = "Item {";
        str += to_string(this->id);
        str += "} ";
        str += this->name;
        return (str);
}

Item* Item::generate() {
        int max_rand = listItemId.size();
        int index = rand() % max_rand;
        int itemId = listItemId[index];
        Item *item = new Item(itemId, listItems.at(itemId));

        return (item);
}

Item* Item::generate(int id) {
        int itemId = id;
        Item *item = new Item(itemId, listItems.at(itemId));
        return (item);
}

void Item::loadItem(vector<string> data) {
    int itemId = atoi(data[0].c_str());
    Item::listItems.insert(make_pair(itemId, data[1]));
    Item::listItemId.push_back(itemId);

}

Potion::Potion() : Item() {

}

Potion::Potion(int id, string name, vector<int> stats) : Item(id, name, "potion", 1) {
    this->hp = stats[0];
    this->mp = stats[1];
    this->damage = stats[2];
    this->armor = stats[3];
    this->power = stats[4];
}

Potion::Potion(int id, string name, int hp, int mp, int damage, int armor) : Item(id, name, "potion", 1), hp(hp), mp(mp), damage(damage), armor(armor)
{

}

Parchemin::Parchemin() : Item() {

}

Parchemin::Parchemin(int id, string name, vector<int> stats) : Item(id, name, "parchemin", 1) {
    this->max_hp = stats[0];
    this->max_mp = stats[1];
}