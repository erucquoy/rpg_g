//
// Created by erucq on 06-08-18.
//

#include "Inventory.h"

int Inventory::addItem (Item *it) {
    for (int i = 0; i < INV_MAX; i++) {
        if (inv[i]->id == 0) {
            inv[i] = it;
            return (i);
        }
    }
    return (-1);
}

int Inventory::check (int id) {
    int result = -1;
    for (int i = 0; i < this->container.size(); i++) {
        if (this->container[i]->id == id) {
            result = i;
            break ;
        }
    }
    return (result);
}

Item* Inventory::get (int index) {
    return (this->container[index]);
}

Item* Inventory::popBack () {
    Item *item;
    item = this->container[this->container.size() - 1];
    this->container.pop_back();
    return (item);
}

int Inventory::add (Item *it) {
    this->container.push_back(it);
    return (this->container.size());
}

int Inventory::count () {
    return (this->container.size());
}

void Inventory::print () {
    cout << "Number of object : " << this->container.size() << endl;
    for (int i = 0; i < this->container.size(); i++) {
        Item* ix = this->container[i];
        ix->toStr();
        cout << ix->id << " = " << ix->name << endl;
    }
    getchar();
}

void Inventory::printInventory () {
    cout << "Your inventory : " << endl;
    for (int i = 0; i < INV_MAX; i++) {
        string res;
        if (inv[i]->id > 0) {
            res = inv[i]->toStr();
        }
        else
            res = "Empty";
        cout << i << " = " << res << endl;
    }
}

void Inventory::removeOne(int id) {
    int pos = this->check(id);
    if (pos >= 0) {
        this->container.erase(this->container.begin() + pos);
    }
}