//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_MOB_H
#define RPGGAME_MOB_H

#include <string>
#include <stdlib.h>
#include <time.h>

#include "Item.h"
#include "Inventory.h"

using namespace std;

enum listMob { tGnome, tGnomeNoir, tGoule, lastMob };

class Mob {
protected:

public:
    int hp = 0;
    int mp = 0;
    int damage = 0;
    int bouclier = 0;
    int max_money = 10;
    int max_loot = 0;
    string name;
    Inventory *inv = new Inventory();

    Mob();
    Mob(int hp_, int mp_, int damage_, int bouclier_);
    Mob(int hp_, int mp_, int damage_, int bouclier_, int max_money, int max_loot);

    int receiveDamage(int dmg);
    static Mob* random();
};

class GardienBasic : public Mob {
public:

    GardienBasic();
};

class Gnome : public Mob {
public:
    Gnome();
};

class Goule : public Mob {
public:
    Goule();
};

class GnomeNoir : public Mob {
public:
    GnomeNoir();
};


#endif //RPGGAME_MOB_H
