//
// Created by edouard on 05/08/18.
//

#ifndef RPGGAME_RPG_H
#define RPGGAME_RPG_H

#include <iostream>
#include <fstream>
#include <istream>
#include <vector>
#include "Context.h"
#include "Item.h"
#include "Inventory.h"
#include "Mob.h"
#include "Personnage.h"
#include "Game.h"


using namespace std;

#define ITEMS_FILENAME "items.txt"

string loadItems ();
const vector<string> explode(const string& s, const char& c);

#endif //RPGGAME_RPG_H
