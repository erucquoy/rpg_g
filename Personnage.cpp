//
// Created by erucq on 06-08-18.
//

#include "Personnage.h"

Personnage::Personnage(int hp_, int mp_) : hp(hp_), mp(mp_), max_hp(hp_), max_mp(mp_)
{
    this->damage = 10;
}

void Personnage::printStats() {
    cout << "Vos stats : " << endl;
    cout << "HP : " << this->hp << " / " << this->max_hp << endl;
    cout << "MP : " << this->mp << " / " << this->max_mp << endl;
    cout << "Power : " << this->power << endl;
    cout << "Damage : " << this->damage << endl;
    cout << "Armor : " << this->bouclier << endl;
    cout << "Money : " << this->money << endl;
}

void Personnage::coupDePoing(Mob *mob) {
    cout << "Vous frappez " << mob->name << " avec une force de " << this->damage << endl;
    int rhp = mob->receiveDamage(this->damage);
    cout << "Il reste " << rhp << " HP a " << mob->name << endl;
}

void Personnage::launchCast(Spell *spell, Mob *mob) {
    if (this->mp >= spell->cost) {
        this->mp -= spell->cost;
        int damage = spell->damage + this->power;
        cout << "Vous lancez " << spell->name << " sur " << mob->name << " avec une puissance de " << damage
             << endl;
        int rhp = mob->receiveDamage(damage);
        cout << "Il reste " << rhp << " HP a " << mob->name << endl;
    }
    else {
        cout << "Vous ratez votre attaque... pas assez de mana" << endl;
    }
}

void Personnage::launchCastHeal(Spell *spell) {
    if (this->mp >= spell->cost) {
        this->mp -= spell->cost;
        int heal = spell->heal + this->power;
        cout << "Vous lancez " << spell->name << " sur vous avec une puissance de " << heal << endl;
        int rhp = this->receiveHeal(heal);
        cout << "Il vous reste " << rhp << " HP" << endl;
    }
    else {
        cout << "Vous ratez votre soin... pas assez de mana" << endl;
    }
}

void Personnage::receiveAttaque(Mob *mob) {
    cout << mob->name << " vous frappe avec une force de " << mob->damage << endl;
    int rhp = this->receiveDamage(mob->damage);
    cout << "Il vous reste " << rhp << " HP" << endl;
}

int Personnage::receiveHeal(int n) {
    if (n + this->hp > this->max_hp) {
        this->hp = this->max_hp;
    } else {
        this->hp += n;
    }
    return (this->hp);
}

int Personnage::receiveDamage(int dmg) {
    dmg = dmg - this->bouclier;

    if (dmg < 0) {
        dmg = 0;
    }

    this->hp -= dmg;

    if (this->hp < 0) {
        this->hp = 0;
    }
    return (this->hp);
}

void Personnage::openChest() {
    cout << "Le coffre s'ouvre doucement ..." << endl;
    int nbItems = (rand() % 4) + 1;

    Inventory *inv = new Inventory();
    int i = 0;
    while (i < nbItems) {
        Item *itemDroped = Item::generate();
        this->inv->add(itemDroped);
        cout << " - " << itemDroped->name << endl;
        i++;
    }

    cout << " ... Le coffre disparait" << endl;
}

void Personnage::receiveDrop(Mob *mob) {
    Item it;

    int nbItems = (rand() % ((mob->max_loot) + 1));

    int start = mob->inv->count();

    while (start < nbItems) {
        mob->inv->add(Item::generate());
        start++;
    }

    cout << "Des objets tombent de la depouille de " << mob->name << " " << nbItems << endl << endl;

    int i = 0;
    int stop = 1;
    if (nbItems < mob->inv->count()) {
        stop = mob->inv->count();
    } else {
        stop = nbItems;
    }

    if (stop > 0) {
        cout << "Vous recevez : " << endl;

        Item *itemDroped;

        for (i = 0; i < stop; i++) {
            itemDroped = mob->inv->popBack();
            cout << " - " << itemDroped->name << endl;
            this->inv->add(itemDroped);
        }

    } else {
        cout << "Vous ne trouvez rien sur la dépouille" << endl;
    }

    int moneyDrop = rand() % mob->max_money;
    cout << "Vous trouvez " << moneyDrop << " pieces d'or" << endl;
    this->money += moneyDrop;

    cout << "Appuyez sur ENTER pour continuer votre aventure" << endl << flush;
    getchar();
}

void Personnage::usePotion(Potion *potion) {
    if (potion->hp != 0) {
        if (potion->hp + this->hp > this->max_hp) {
            this->hp = this->max_hp;
        } else {
            this->hp += potion->hp;
        }
    }
    if (potion->mp != 0) {
        if (potion->mp + this->mp > this->max_mp) {
            this->mp = this->max_mp;
        } else {
            this->mp += potion->mp;
        }
    }
    if (potion->damage != 0) {
        this->damage += potion->damage;
    }
    if (potion->armor != 0) {
        this->bouclier += potion->armor;
    }
    if (potion->power != 0) {
        this->power += potion->power;
    }
}

void Personnage::useParchemin(Parchemin *parcho) {
    if (parcho->max_hp != 0) {
        this->max_hp += parcho->max_hp;
    }
    if (parcho->max_mp != 0) {
        this->max_mp += parcho->max_mp;
    }
}

void Personnage::useItem(int choix) {
    Item *item = this->inv->container[choix];

    if (this->ctx->isPotion(item->id) == 1) {
        Potion *potion = new Potion(item->id, item->name, this->ctx->getStats(item->id));
        this->usePotion(potion);
        this->inv->container.erase(this->inv->container.begin() + choix);
    }
    if (this->ctx->isParchemin(item->id) == 1) {
        Parchemin *parcho = new Parchemin(item->id, item->name, this->ctx->getStats(item->id));
        this->useParchemin(parcho);
        this->inv->container.erase(this->inv->container.begin() + choix);
    }
    else {
        cout << "Cette object n'est pas utilisable" << endl;
    }
    /*cout << Potion::listPotion.size() << " size" << endl;
    if (Potion::listPotion.find(item->id) == Potion::listPotion.end()) {
        cout << "Cet objet n'est pas utilisable" << endl;
    }
    else {
        cout << "Vous utilisez une " << item->name << endl;
        Potion popo = Potion::listPotion.at(item->id);
        cout << popo.hp << popo.mp << popo.damage << popo.armor << popo.name << endl;
        this->usePotion(&popo);
    }*/
}

void Personnage::seeInventory () {
    cout << "Entrez le numéro devant l'objet pour intéragir avec." << endl;
    cout << "Voici votre inventaire : " << endl;
    int i = 0;
    Item *it;
    for (i = 0; i < this->inv->container.size(); i ++) {
        it = this->inv->container[i];
        cout << i << ". " << it->name << endl;
    }
    string input;
    int quit = 0;
    int choix = -1;
    while (!(choix >= 0 && choix < this->inv->container.size())) {
        cout << "Tapez votre choix : " << endl;
        cin >> input;
        if (input == "q") {
            quit = 1;
            break;
        }
        choix = atoi(input.c_str());
        cout << choix <<endl;
    }
    if (quit == 0) {
        this->useItem(choix);
    }
}

Elfe::Elfe() : Personnage(90, 120)
{

}

Nain::Nain() : Personnage(120, 80)
{

}

Humain::Humain() : Personnage(105, 105)
{

}

Geant::Geant() : Personnage(190, 10)
{

}

Gobelin::Gobelin() : Personnage(60, 160)
{

}

int Elfe::cast(Mob *mob) {
    cout << "Choisis ton sort : (" << this->mp << " MP)" << endl;
    cout << "a. Boule de Feu" << endl;
    cout << "z. Tempete de Flammes" << endl;
    cout << "e. Soin Mineur" << endl;
    cout << "r. Soin Majeur" << endl;

    string input = "0";
    while (!(input == "a" || input == "z" || input == "e" || input == "r")) {
        cin >> input;
        cout << input;
    }

    if (input == "a") {
        launchCast(new BouleDeFeu(), mob);
    }
    else if (input == "z") {
        launchCast(new TempeteDeFlamme(), mob);
    }
    else if (input == "e") {
        launchCastHeal(new SoinMineur());
    }
    else if (input == "r") {
        launchCastHeal(new SoinMajeur());
    }
    return (0);
}

int Gobelin::cast(Mob *mob) {
    cout << "Choisis ton sort : (" << this->mp << " MP)"  << endl;
    cout << "a. Eclair" << endl;
    cout << "z. Tempete de Flammes" << endl;
    cout << "e. Soin Mineur" << endl;
    cout << "r. Soin Majeur" << endl;

    string input = "0";
    while (!(input == "a" || input == "z" || input == "e" || input == "r")) {
        cin >> input;
        cout << input;
    }

    if (input == "a") {
        launchCast(new Eclair(), mob);
    }
    else if (input == "z") {
        launchCast(new TempeteDeFlamme(), mob);
    }
    else if (input == "e") {
        launchCastHeal(new SoinMineur());
    }
    else if (input == "r") {
        launchCastHeal(new SoinMajeur());
    }
    return (0);
}

int Geant::cast(Mob *mob) {

    return (0);
}

int Nain::cast(Mob *mob) {

    return (0);
}

int Personnage::cast(Mob *mob) {

    return (0);
}

int Humain::cast(Mob *mob) {

    return (0);
}