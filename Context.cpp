//
// Created by erucq on 08-08-18.
//

#include "Context.h"

Context::Context() {

}

void Context::putId(int id) {
    this->listItemId.push_back(id);
}

void Context::putName(int id, string name) {
    this->mapItemName.insert(make_pair(id, name));
}

void Context::putType(int id, string type) {
    this->mapItemType.insert(make_pair(id, type));
}

void Context::createItem(int id, string name, string type) {
    this->putId(id);
    this->mapItemName.insert(make_pair(id, name));
    this->mapItemType.insert(make_pair(id, type));
    vector<int> stats = { 0, 0, 0, 0, 0 };
    this->mapItemStats.insert(make_pair(id, stats));
}

void Context::createItem(int id, string name, string type, vector<int> stats) {
    this->putId(id);
    this->mapItemName.insert(make_pair(id, name));
    this->mapItemType.insert(make_pair(id, type));
    this->mapItemStats.insert(make_pair(id, stats));
}

string Context::getType(int id) {
    if (this->mapItemType.find(id) != this->mapItemType.end()) {
        return (this->mapItemType.at(id));
    }
    return ("");
}

vector<int> Context::getStats(int id) {
    vector<int> nuub;
    if (this->mapItemStats.find(id) != this->mapItemStats.end()) {
        return (this->mapItemStats.at(id));
    }
    return (nuub);
}

string Context::getName(int id) {
    if (this->mapItemName.find(id) != this->mapItemName.end()) {
        return (this->mapItemName.at(id));
    }
    return ("");
}

int Context::isPotion(int id) {
    if (this->getType(id) == "potion") {
        return (1);
    }
    return (0);
}
int Context::isParchemin(int id) {
    if (this->getType(id) == "parchemin") {
        return (1);
    }
    return (0);
}
