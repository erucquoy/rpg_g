//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_SPELL_H
#define RPGGAME_SPELL_H

#include <iostream>

using namespace std;

class Spell {

public:
    int cost = 0;
    int damage = 0;
    int heal = 0;
    string name = "Undefined Spell";
    Spell();
    Spell(string name, int cost, int damage);
    Spell(string name, int cost, int damage, int heal);
};

class BouleDeFeu : public Spell {
public:
    BouleDeFeu();
};

class TempeteDeFlamme: public Spell {
public:
    TempeteDeFlamme();
};

class Eclair : public Spell {
public:
    Eclair();
};

class SoinMineur : public Spell {
public:
    SoinMineur();
};

class SoinMajeur : public Spell {
public:
    SoinMajeur();
};


#endif //RPGGAME_SPELL_H
