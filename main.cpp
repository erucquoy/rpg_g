#include "rpg.h"

string loadItems (Context *ctx) {
    string line, content;
    ifstream file ("items.txt");
    int nbItems = 0;
    if (file.is_open())
    {
        while ( getline (file,line) )
        {
            vector<string> exp = explode(line, ',');
            Item::loadItem(exp);
            int itemId = atoi(exp[0].c_str());
            string itemName = exp[1];
            string itemType = exp[2];
            string itemStats = "";
            vector<int> itemTT;
            for (int i = 3; i < exp.size(); i++) {
                itemTT.push_back(atoi(exp[i].c_str()));
            }
            cout << itemStats << endl;
            ctx->createItem(itemId, itemName, itemType, itemTT);

            nbItems++;
        }
        file.close();
    }
    cout << "=> " << nbItems << " items loaded" << endl;
    return (content);
}

int main() {
    srand(time(NULL));
    Context *ctx = new Context();

    Game *game = new Game();
    game->ctx = ctx;

    loadItems(ctx);

    game->choixPersonnage();
    game->createGame();

    return 0;
}



const vector<string> explode(const string& s, const char& c)
{
    string buff{""};
    vector<string> v;

    for(auto n:s)
    {
        if(n != c) buff+=n; else
        if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);

    return v;
}

