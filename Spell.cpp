//
// Created by erucq on 06-08-18.
//

#include "Spell.h"

Spell::Spell() {

}

Spell::Spell(string name, int cost, int damage) : name(name), cost(cost), damage(damage) {

}

Spell::Spell(string name, int cost, int damage, int heal) : name(name), cost(cost), damage(damage), heal(heal) {

}

BouleDeFeu::BouleDeFeu() : Spell("Boule de Feu", 10, 15) {}

TempeteDeFlamme::TempeteDeFlamme() : Spell("Tempete de Flamme", 50, 150) {}

Eclair::Eclair() : Spell("Eclair", 20, 36) {}

SoinMineur::SoinMineur() : Spell("Soin Mineur", 15, 0, 15) {}

SoinMajeur::SoinMajeur() : Spell("Soin Majeur", 35, 0, 50) {}
