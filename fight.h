//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_FIGHT_H
#define RPGGAME_FIGHT_H

#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include "Personnage.h"
#include "Mob.h"

#define FIGHT_RESULT_WIN 2
#define FIGHT_RESULT_LOSE 4
#define FIGHT_RESULT_GIVEUP 5

int fight (Personnage *hero, Mob *mob);
void showActions();
int tryToGiveUp(Personnage *hero, Mob *mob);

#endif //RPGGAME_FIGHT_H
