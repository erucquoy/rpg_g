//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_ITEM_H
#define RPGGAME_ITEM_H

#include <iostream>
#include <vector>
#include <map>

#include "Context.h"

using namespace std;

class Item {
public:
    string name;
    int id = 0;
    string type;
    int usable = 0;

    static map<int, string> listItems;
    static map<int, string> listItemType;
    static vector<int> listItemId;


    string toStr();
    int use ();
    Item();
    Item(int id);
    Item(int id, string name);
    Item(int id, string name, string type);
    Item(int id, string name, string type, int usable);
    static Item * generate();
    static Item * generate(int id);


    static void loadItem(vector<string> item);
};

class Potion : public Item {
public:
    static map<int, Potion> listPotion;
    int hp = 0;
    int mp = 0;
    int damage = 0;
    int armor = 0;
    int power = 0;
    Potion();
    Potion(int id, string name, vector<int> stats);
    Potion(int id, string name, int hp, int mp, int damage, int armor);

    static void put(int id, Potion po);
    static Potion get(int id);
};

class Parchemin : public Item {
public:
    int max_hp = 0;
    int max_mp = 0;
    Parchemin();
    Parchemin(int id, string name, vector<int> stats);
};

#endif //RPGGAME_ITEM_H
