//
// Created by erucq on 06-08-18.
//

#ifndef RPGGAME_INVENTORY_H
#define RPGGAME_INVENTORY_H

#include <iostream>
#include <vector>
#include "Item.h"

using namespace std;

#define INV_MAX 12

class Inventory {
public:
    Item* inv[INV_MAX];
    vector<Item*> container;

    int addItem (Item *it);
    int add (Item *it);
    int count ();
    int check (int id);
    void removeOne(int id);
    void print ();
    void printInventory ();
    Item* get(int index);
    Item* popBack();
};


#endif //RPGGAME_INVENTORY_H
