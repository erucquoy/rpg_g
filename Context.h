//
// Created by erucq on 08-08-18.
//

#ifndef RPGGAME_CONTEXT_H
#define RPGGAME_CONTEXT_H

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Context {
public:
    map<int, string> mapItemName;
    map<int, string> mapItemType;
    map<int, vector<int>> mapItemStats;
    vector<int> listItemId;

    void putName(int id, string name);
    void putType(int id, string type);
    void putStat(int id, vector<int> stats);
    void putId(int id);
    void createItem(int id, string name, string type);
    void createItem(int id, string name, string type, vector<int> stats);
    int isPotion(int id);
    int isParchemin(int id);

    string getName(int id);
    string getType(int id);
    vector<int> getStats(int id);

    Context();
};


#endif //RPGGAME_CONTEXT_H
